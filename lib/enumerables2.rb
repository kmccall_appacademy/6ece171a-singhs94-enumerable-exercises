require 'byebug'

# EASY

# Define a method that returns the sum of all the elements in its argument (an
# array of numbers).
def array_sum(arr)
  return arr.reduce(:+) if arr.empty? == false
  0
end

# Define a method that returns a boolean indicating whether substring is a
# substring of each string in the long_strings array.
# Hint: you may want a sub_tring? helper method
def in_all_strings?(long_strings, substring)
  long_strings.each do |string|
    if string.include?(substring) == false
      return false
    end
  end
  return true
end

# Define a method that accepts a string of lower case words (no punctuation) and
# returns an array of letters that occur more than once, sorted alphabetically.
def non_unique_letters(string)
  #turn string into array of letters
  array = string.split(' ').join('').split('')
  #filter array for duplicates
  array.select! do |letter|
    if array.count(letter) > 1
      letter
    end
  end
  #remove duplicates
  return array.uniq!
end

# Define a method that returns an array of the longest two words (in order) in
# the method's argument. Ignore punctuation!
def longest_two_words(string)
  array = string.split
  array = array.sort_by {|x| x.length}
  array[array.length-2..-1]
end

# MEDIUM

# Define a method that takes a string of lower-case letters and returns an array
# of all the letters that do not occur in the method's argument.
def missing_letters(string)
  alphabet = 'a'.upto('z').to_a
  alphabet.select! do |letter|
    if string.include?(letter) == false
      letter
    end
  end
  alphabet
end

# Define a method that accepts two years and returns an array of the years
# within that range (inclusive) that have no repeated digits. Hint: helper
# method?
def no_repeat_years(first_yr, last_yr)
  years = first_yr.upto(last_yr).to_a
  years.select! do |year|
    if not_repeat_year?(year)
      year
    end
  end
  years
end

def not_repeat_year?(year)
  array = year.to_s.split('')
  if array == array.uniq
    return true
  end
  return false
end

# HARD

# Define a method that, given an array of songs at the top of the charts,
# returns the songs that only stayed on the chart for a week at a time. Each
# element corresponds to a song at the top of the charts for one particular
# week. Songs CAN reappear on the chart, but if they don't appear in consecutive
# weeks, they're "one-week wonders." Suggested strategy: find the songs that
# appear multiple times in a row and remove them. You may wish to write a helper
# method no_repeats?
def one_week_wonders(songs)
  songs.select! do |s|
    if no_repeats?(s, songs)
      s
    end
  end
  songs.uniq!
end

def no_repeats?(song_name, songs)
  i = 0
  while i < songs.length
    if songs[i] == song_name
      return false if songs[i+1] == song_name || songs[i-1] == song_name
    end
    i += 1
  end
  return true
end

# Define a method that, given a string of words, returns the word that has the
# letter "c" closest to the end of it. If there's a tie, return the earlier
# word. Ignore punctuation. If there's no "c", return an empty string. You may
# wish to write the helper methods c_distance and remove_punctuation.

def for_cs_sake(string)
  array = string.split(' ')
  array2 = array.map do |word|
    word = remove_punctuation(word)
  end
  array2 = array2.sort_by {|x| c_distance(x)}
  array2[0]

end

def c_distance(word)
  distance = 0
  word = word.split('')
  word.reverse.each do |letter|
    if letter == 'c'
      return distance
    end
    distance += 1
  end
  return 100
end

def remove_punctuation(word)
  array = word.split('')
  alphabet = 'a'.upto('z').to_a
  array.select! {|x| x if alphabet.include?(x.downcase)}
  array.join('')
end

# Define a method that, given an array of numbers, returns a nested array of
# two-element arrays that each contain the start and end indices of whenever a
# number appears multiple times in a row. repeated_number_ranges([1, 1, 2]) =>
# [[0, 1]] repeated_number_ranges([1, 2, 3, 3, 4, 4, 4]) => [[2, 3], [4, 6]]

def repeated_number_ranges(arr)
  answer = []
  i = 0
  while i < arr.length
    next_v = arr[i+1]
    if arr[i] == next_v
      bool = true
      start_index = i
      while start_index < arr.length
        start_index += 1
        if arr[start_index] == arr[i]
          end_index = start_index
        else
          break
        end
      end
      answer.push([i,end_index]) if bool == true
      bool = false
      i = end_index 
    end
    i += 1
  end
  answer
end
